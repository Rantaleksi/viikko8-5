/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko8_5;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Aleksi
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Button saveButton;
    @FXML
    private TextArea inputField;
    @FXML
    private Button loadButton;
    @FXML
    private TextField saveFileName;
    @FXML
    private TextField loadFileName;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void saveButtonAction(ActionEvent event) {
        ReadAndWriteIO rawio = new ReadAndWriteIO(saveFileName.getText());
        try {
            rawio.writeText(inputField.getText());
        } catch (IOException ex) {
            System.out.println("Jokin meni vikaan.");
        }
    }

    @FXML
    private void loadButtonAction(ActionEvent event) {
        ReadAndWriteIO rawio = new ReadAndWriteIO(loadFileName.getText());
        try {
            inputField.setText(inputField.getText() + " " + rawio.readText());
        } catch (IOException ex) {
            System.out.println("Jokin meni vikaan.");
        }
    }

}
