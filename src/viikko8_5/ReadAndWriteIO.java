/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko8_5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Aleksi
 */
public class ReadAndWriteIO {
    private String filename;
    
    public ReadAndWriteIO(String s) {
        filename = s;
    }
    
    // Writes to txt file
    public void writeText(String text) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(filename));
        out.write(text);
        out.close();
    }
    
    // Reads from txt file
    public String readText() throws FileNotFoundException, IOException{
        BufferedReader in = new BufferedReader(new FileReader(filename));
        String s = in.readLine();
        in.close();
        return s;
    }
    
    // Reads whole txt file
    public void readFile(String filename) throws FileNotFoundException, IOException{
        BufferedReader in = new BufferedReader(new FileReader(filename));
        String line;
        while((line = in.readLine()) != null){
            System.out.println(line);
        }
    }
}
